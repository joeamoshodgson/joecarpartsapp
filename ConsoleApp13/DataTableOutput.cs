﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class DataTableOutput
{

    public DataTable MakeDataTable()
    {
        DataTable dt = new DataTable();
        //dt.Clear();
        dt.Columns.Add("Part Number");
        dt.Columns.Add("Manufacturer");
        dt.Columns.Add("Price");
        dt.Columns.Add("Product Type");
        dt.Columns.Add("Currency");
        dt.AcceptChanges();
        return dt;

    }
  

}
