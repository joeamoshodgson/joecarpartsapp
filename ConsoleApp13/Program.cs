﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Program
{

    static void Main(string[] args)
    {

        DataTableOutput table = new DataTableOutput();
        DataTable dt = table.MakeDataTable();
        
        //GaudinTools tools = new GaudinTools();
        //HtmlDocument htmlDocVariable = tools.GaudinGrabHtml();
        
        List<String> listOfProductIds = new List<string>();
        listOfProductIds.Add("97057311100");
        listOfProductIds.Add("97057311100");
        //listOfProductIds.Add("97057311100");


         foreach (var productID in listOfProductIds)
         {

            GaudinTools tools = new GaudinTools();
            HtmlDocument htmlDocVariable = tools.GaudinGrabHtml(productID);

            string productIDadd = "97057311100";
            string price = tools.Gaudinprice(htmlDocVariable);
            string manufacturer = tools.Gaudinmanufacturer(htmlDocVariable);
            string productType = tools.Gaudinproducttype(htmlDocVariable);
            string currency = tools.Gaudincurrency();
            dt.Rows.Add(productIDadd, manufacturer, price, productType, currency);

         }
        
        Console.ReadLine();

    }

}

