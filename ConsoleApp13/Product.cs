﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Product
{
    public string PartNumber { get; set; }
    public string Manufacturer { get; set; }
    public string Price { get; set; }
    public string ProductType { get; set; }
    public string Currency { get; set; }

}
