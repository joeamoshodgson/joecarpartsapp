﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;


class GrabHtmlFile
{
     public HtmlDocument GrabHtml(string html)
     {
        //Console.WriteLine(html);

        HtmlWeb web = new HtmlWeb();

        HtmlDocument htmlDoc = web.Load(html);

        return htmlDoc;

     }


}

