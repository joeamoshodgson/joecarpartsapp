﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Data;

public class GaudinTools
{

    public HtmlDocument GaudinGrabHtml(string ProductID)
    {

        string html = @"https://www.gaudinporscheparts.com/?p=catalog&mode=search&search_in=all&search_str=" + ProductID;

        GrabHtmlFile grab = new GrabHtmlFile();
        HtmlDocument GaudinhtmlDoc = grab.GrabHtml(html);
             
        Console.WriteLine(html);

        return GaudinhtmlDoc;
            //    
    }


    public string Gaudinmanufacturer(HtmlDocument htmlDoc)

    {
        
        HtmlNode GaudinManufacturernode = htmlDoc.DocumentNode.SelectSingleNode(@"//span[@class='fitment-make']");

        ConvertTrimStrings convert = new ConvertTrimStrings();
        string GaudinManufacturer = convert.ConverTrimStrings(GaudinManufacturernode);
        
        Console.WriteLine(GaudinManufacturer);
        
        
        return GaudinManufacturer;
        
    }

    public string Gaudinprice(HtmlDocument htmlDoc)
    {

        HtmlNode GaudinPricenode = htmlDoc.DocumentNode.SelectSingleNode("//*[@id=\"catalogView\"]/div[1]/div/div[2]/div[3]/div[2]");

        ConvertTrimStrings convert = new ConvertTrimStrings();
        string GaudinPrice = convert.ConverTrimStrings(GaudinPricenode);

        Console.WriteLine(GaudinPrice);
        return GaudinPrice;
               
    }
    
    public string Gaudinproducttype(HtmlDocument htmlDoc)
    {
        HtmlNode GaudinProductTypenode = htmlDoc.DocumentNode.SelectSingleNode("//*[@id=\"catalogView\"]/div[1]/div/div[2]/h5/a");

        ConvertTrimStrings convert = new ConvertTrimStrings();
        string GaudinProductType = convert.ConverTrimStrings(GaudinProductTypenode);

        Console.WriteLine(GaudinProductType);

        return GaudinProductType;
        //return GaudinProductTypenode;
        

    }

    public string Gaudincurrency()
    {
        string GaudinCurrency = "$";

        Console.WriteLine(GaudinCurrency);

        return GaudinCurrency;

    }
           
  
}
