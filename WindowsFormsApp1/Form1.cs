﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private Form1 form;

        public Form1(Form form1)
        {
            //InitializeComponent();
            this.form = form1;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            DataTableOutput table = new DataTableOutput();
            DataTable dt = table.MakeDataTable();

            //GaudinTools tools = new GaudinTools();
            //HtmlDocument htmlDocVariable = tools.GaudinGrabHtml();

            List<String> listOfProductIds = new List<string>();
            listOfProductIds.Add("97057311100");
            listOfProductIds.Add("97057311100");
            //listOfProductIds.Add("97057311100");


            foreach (var productID in listOfProductIds)
            {

                GaudinTools tools = new GaudinTools();
                HtmlAgilityPack.HtmlDocument htmlDocVariable = tools.GaudinGrabHtml(productID);

                string productIDadd = "97057311100";
                string price = tools.Gaudinprice(htmlDocVariable);
                string manufacturer = tools.Gaudinmanufacturer(htmlDocVariable);
                string productType = tools.Gaudinproducttype(htmlDocVariable);
                string currency = tools.Gaudincurrency();
                dt.Rows.Add(productIDadd, manufacturer, price, productType, currency);

            }


            form.dataGridView1.DataSource = dt;

            Console.ReadLine();



        }
    }
}
